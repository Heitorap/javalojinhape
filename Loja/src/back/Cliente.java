package back;

public class Cliente implements Comprar{
	private String nome;
	private int cpf;
	private int idade;
	private int ncalçado;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getCpf() {
		return cpf;
	}
	public void setCpf(int cpf) {
		this.cpf = cpf;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public int getNcalçado() {
		return ncalçado;
	}
	public void setNcalçado(int ncalçado) {
		this.ncalçado = ncalçado;
	}
	
	public void comprar() {
		System.out.println("Compra");
	}
	
}
